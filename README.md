Unofficial brux gdk engine fork to achieve:

 - Better performance
 - Fixed frametime in main.cpp
 - Cleaner code
 - More error checking in cjson.c

Now that this is done, I'll try to match the unstable branch and fix the errors that contains.
I'll pack what I have now as appimage too.

