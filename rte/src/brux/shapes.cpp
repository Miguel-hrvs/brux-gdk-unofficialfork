//  Brux - Shapes
//  Copyright (C) 2016 KelvinShadewing
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*=============*\
| SHAPES SOURCE |
\*=============*/

#include "brux/main.hpp"
#include "brux/global.hpp"
#include "brux/maths.hpp"
#include "brux/shapes.hpp"
#include <cmath>

/*\
This part is being put on hold.
It's being implemented in Squirrel for now.
Once all the math is worked out, it will be
moved here to C++. How shapes are addressed
will also be changed to suit the new system.

Some functions will still need to be done in
Squirrel, just to make them easier, but all
the heavy math will be done here for
performance reasons.
\*/

///////////
// POINT //
///////////

//Constructors
xyPnt::xyPnt() {
	x = 0;
	y = 0;
};

xyPnt::xyPnt(float _x, float _y) {
	x = _x;
	y = _y;
};

xyPnt::xyPnt(const xyPnt& v) {
	x = v.x;
	y = v.y;
};

//Assignment Operators
xyPnt& xyPnt::operator = (const xyPnt& v) {
	x = v.x;
	y = v.y;

	return *this;
};

xyPnt& xyPnt::operator += (const xyPnt& v) {
	x += v.x;
	y += v.y;

	return *this;
};

xyPnt& xyPnt::operator -= (const xyPnt& v) {
	x -= v.x;
	y -= v.y;

	return *this;
};

xyPnt& xyPnt::operator *= (const float s) {
	x *= s;
	y *= s;

	return *this;
};

xyPnt& xyPnt::operator /= (const float s) {
	x /= s;
	y /= s;

	return *this;
};

//Comparison Operators
bool xyPnt::operator == (const xyPnt& v) {
	return(x == v.x && y == v.y);
};

bool xyPnt::operator != (const xyPnt& v) {
	return(x != v.x || y != v.y);
};

//Binary operators
const xyPnt xyPnt::operator + (const xyPnt& v) {
	xyPnt result(*this);
	result += v;
	return result;
};

const xyPnt xyPnt::operator - (const xyPnt& v) {
	xyPnt result(*this);
	result -= v;
	return result;
};

const xyPnt xyPnt::operator * ( const float& s ) {
	xyPnt result( *this );
	result *= s;
	return result;
};

const xyPnt xyPnt::operator / ( const float& s ) {
	xyPnt result( *this );
	result /= s;
	return result;
};

//Access operator
const float xyPnt::operator[](const int& i) {
	switch(i) {
		case 0: return x;
		case 1: return y;
		default: return -1;
	}
};

//Functions
float xyPnt::getLength() {
	return(sqrt((x * x) + (y * y)));
};

void xyPnt::setLength(float l) {
    float h = getLength();
    if (h != 0) {
        float ratio = l / h;
        x *= ratio;
        y *= ratio;
    }
};

const float RAD_TO_DEG = 180 / M_PI;

void xyPnt::rotate(float angle) {
	float theta = angle * (RAD_TO_DEG);
	float nx = (x * cos(theta)) - (y * sin(theta));
	float ny = (x * sin(theta)) + (y * cos(theta));
	x = nx;
	y = ny;
};

void xyPnt::rotate(float angle, float pivx, float pivy) {
    // Offset the vector
    x -= pivx;
    y -= pivy;

    // Rotate the vector
    float theta = angle * (RAD_TO_DEG);
    float nx = x * cos(theta) - y * sin(theta);
    float ny = x * sin(theta) + y * cos(theta);
    x = nx;
    y = ny;

    // Reset the vector
    x += pivx;
    y += pivy;
};


float xyPnt::getArea() {
	return 0;
};

float xyPnt::dot(xyPnt* p) {
    if (p == nullptr) {
        // Handle the error appropriately, e.g., throw an exception, return a default value, etc.
        throw std::invalid_argument("Null pointer passed to dot function");
    }
    return (x * p->x) + (y * p->y);
};

///////////
// SHAPE //
///////////

xyShape::xyShape(float _x, float _y, float _a, int _type) {
	x = _x;
	y = _y;
	a = _a;
	type = _type;
};

bool xyLineLine(xyPnt* a, xyPnt* b, xyPnt* c, xyPnt* d) {
    // Check if pointers are null
    if (a == nullptr || b == nullptr || c == nullptr || d == nullptr) {
        throw std::invalid_argument("Null pointer passed to xyLineLine function");
    }

    // Calculate the denominators
    float denom = ((b->x - a->x) * (d->y - c->y)) - ((b->y - a->y) * (d->x - c->x));
    float nume0 = ((a->y - c->y) * (d->x - c->x)) - ((a->x - c->x) * (d->y - c->y));
    float nume1 = ((a->y - c->y) * (b->x - a->x)) - ((a->x - c->x) * (b->y - a->y));

    // If lines are parallel
    if(denom == 0) return nume0 == 0 && nume1 == 0;

    // Calculate the intersection point
    float r = nume0 / denom;
    float s = nume1 / denom;

    // Check if the intersection point lies on both lines
    return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
};

bool xyPointLine(xyPnt* a, xyPnt* b, xyPnt* c) {
    // Check if pointers are null
    if (a == nullptr || b == nullptr || c == nullptr) {
        throw std::invalid_argument("Null pointer passed to xyPointLine function");
    }

    // Calculate the cross product of vectors AB and AC
    float crossproduct = (c->y - a->y) * (b->x - a->x) - (c->x - a->x) * (b->y - a->y);

    // If cross product is not zero, then point C is not on the line.
    if (crossproduct != 0)
        return false;

    // Calculate the dot product of vectors AB and AC
    float dotproduct = (c->x - a->x) * (b->x - a->x) + (c->y - a->y)*(b->y - a->y);

    // If dot product is negative, then point C is not on the line segment.
    if (dotproduct < 0)
        return false;

    // Calculate the squared length of vector AB
    float squaredlengthba = (b->x - a->x)*(b->x - a->x) + (b->y - a->y)*(b->y - a->y);

    // If dot product is greater than squared length of BA, then point C is not on the line segment.
    if (dotproduct > squaredlengthba)
        return false;

    // If none of the above conditions are met, then point C is on the line segment.
    return true;
};

bool xyLineCircle(xyPnt* a, xyPnt* b, xyPnt* c, float r) {
    // Check if pointers are null
    if (a == nullptr || b == nullptr || c == nullptr) {
        throw std::invalid_argument("Null pointer passed to xyLineCircle function");
    }

    // Check if points A or B are inside the circle
    if(xyDistance(a->x, a->y, c->x, c->y) <= r) return true;
    if(xyDistance(b->x, b->y, c->x, c->y) <= r) return true;

    // Calculate the distance between points A and B
    float distX = a->x - b->x;
    float distY = a->y - b->y;
    float len = sqrt((distX * distX) + (distY * distY));

    // Calculate the dot product
    float dot = ( ((c->x - a->x) * (b->x - a->x)) + ((c->y - a->y) * (b->y - a->y)) ) / pow(len, 2);

    // Calculate the closest point on the line to point C
    float cx = a->x + (dot * (b->x - a->x));
    float cy = a->y + (dot * (b->y - a->y));

    // Check if the closest point is on the line segment
    if (!xyLinePoint(a->x, a->y, b->x, b->y, cx, cy)) return false;

    // Calculate the distance from the closest point to point C
    distX = cx - c->x;
    distY = cy - c->y;
    len = sqrt((distX * distX) + (distY * distY));

    // Check if the closest point is inside the circle
    return (len <= r);
};

bool xyLinePoint(float lx0, float ly0, float lx1, float ly1, float px, float py) {
    // Calculate the distances between points
    float ll = xyDistance(lx0, ly0, lx1, ly1);
    float d1 = xyDistance(lx0, ly0, px, py);
    float d2 = xyDistance(px, py, lx1, ly1);

    // Define a small tolerance value to account for floating point precision
    const float tolerance = 1e-5;

    // Check if the sum of distances from the point to the line endpoints is equal to the line length
    return (d1 + d2 <= ll + tolerance && d1 + d2 >= ll - tolerance);
};

bool xyHitTest(xyShape* a, xyShape* b) {
    switch(a->type) {
        case _LIN:
            if(b->type == _LIN) {
                return xyLineLine(a->pnt[0], a->pnt[1], b->pnt[0], b->pnt[1]);
            }
            break;
        case _CIR:
            if(b->type == _CIR) {
                return (xyDistance(a->x, a->y, b->x, b->y) <= a->a + b->a);
            }
            break;
        // Add more cases here as you implement them
    };
    return 0;  // Default case if no other conditions are met
};
