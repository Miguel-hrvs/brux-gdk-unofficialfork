//  Brux - Main
//  Copyright (C) 2016 KelvinShadewing
//  Copyright (C) 2023 hexaheximal
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*=============================================*\
| PROJECT: Brux Game Runtime Environment        |
| AUTHOR: Nick Kovacs                           |
| DATE: 8-15-15                                 |
| DESC: Runtime environment used for            |
|  games and applications created using         |
|  the Brux game framework.                     |
| LICENSE: GNU Affero General Public License v3 |
\*=============================================*/

/*===========*\
| MAIN SOURCE |
\*===========*/

#include "brux/main.hpp"

#include "audio/audio.hpp"
#include "brux/core.hpp"
#include "brux/fileio.hpp"
#include "brux/global.hpp"
#include "brux/graphics.hpp"
#include "brux/input.hpp"
#include "squirrel/wrapper.hpp"

/////////////////
//MAIN FUNCTION//
/////////////////

#ifdef __EMSCRIPTEN__
EMSCRIPTEN_KEEPALIVE
#endif
int main(int argc, char* argv[]) {
#ifdef __EMSCRIPTEN__
	EM_ASM(
		console.log(FS.readdir("/"));
		console.log(FS.readdir("/bin"));
		FS.chdir('/bin');
	);
#endif
	// Initiate everything
	
	int initResult = 0;
	try {
		initResult = xyInit();
	}
	catch (std::exception& err) {
		xyPrint(0, "Error initiating Brux: %s", err.what());
		xyEnd();
		return 1;
	}
	if (initResult == 0) {
		xyPrint(0, "Failed to initiate Brux!");
		xyEnd();
		return 1;
	}

	// Process arguments
	
	std::string xygapp = "";
	std::string curarg = "";
	
	for (int i = 0; i < argc; i++) {
		curarg = argv[i];
		
		// The first argument is just the
		// command to invoke the runtime,
		// so skip it.
		
		if (i != 0) {
			// Handle arguments

			if (curarg == "-f" || curarg == "--fullscreen") {
				SDL_SetWindowFullscreen(gvWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
			}
			
			// Check if the argument is long enough to be a source file
				
			if (curarg.length() > 4) {
				// Check if the argument is a source file
				
				if (curarg.substr(curarg.find_last_of('.')) == ".sq" || curarg.substr(curarg.find_last_of('.')) == ".nut" || curarg.substr(curarg.find_last_of('.')) == ".brx") {
					// Check if the file actually exists
					
					if (xyLegacyFileExists(curarg)) {
						// If everything looks good, set it as the main file.
						
						xygapp = curarg;
						
						size_t found = xygapp.find_last_of("/\\");
						
						gvWorkDir = xygapp.substr(0, found);
						
						if (chdir(gvWorkDir.c_str()) != 0) {
							xyPrint(0, "Error initiating Brux: Cannot change to input file working directory: %d", errno);
							xyEnd();
							return 1;
						}
						
						const std::string curdir = xyGetDir();
						
						xyPrint(0, "Working directory: %s", curdir.c_str());
					}
				}
			}
		}
	}

	SDL_ShowCursor(0);

	// Mount the current working directory.
	 
	xyFSMount(xyGetDir(), "/", true);

	// Set the current write directory to a default for Brux.
	// Can be changed later by the game.
	
	xySetWriteDir(xyGetPrefDir("brux", "brux"));

	// If the filename isn't blank, run it.
	
	if (xygapp != "") {
		xyPrint(0, "Running %s...", xygapp.c_str());
		sqstd_dofile(gvSquirrel, xygapp.c_str(), 0, 1);
	} else {
		// Otherwise, attempt to load test.nut or game.brx as a fallback.
		
		if (xyFileExists("test.nut")) {
			sqstd_dofile(gvSquirrel, "test.nut", 0, 1);
		}
		
		else if (xyFileExists("game.brx")) {
			sqstd_dofile(gvSquirrel, "game.brx", 0, 1);
		}
	}

	// End game
	
	try {
		xyEnd();
	}
	catch (std::exception& err) {
		xyPrint(0, "Error quitting Brux: %s", err.what());
		return 1;
	}

	return 0;
}

///////////////////
//OTHER FUNCTIONS//
///////////////////

// Handles initialization of SDL2 and Squirrel

const int SQ_STACK_SIZE = 1024;
const int RENDERER_COLOR = 0xFF;

int xyInit() {
    // Initiate log file
    remove("log.txt");
    gvLog.open("log.txt", ios_base::out);

    // Print opening message
    xyPrint(0, "\n/========================\\\n| BRUX GAME RUNTIME LOG |\n\\========================/\n\n");
    xyPrint(0, "Initializing program...\n\n");

    // Initialize the file system (PhysFS)
    xyPrint(0, "Initializing file system...");
    xyFSInit();

    // Initiate SDL2
    SDL_SetHint(SDL_HINT_XINPUT_ENABLED, "0");
#ifdef __EMSCRIPTEN__
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER | SDL_INIT_EVENTS) < 0) {
#else
    if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
#endif
        throw std::runtime_error("Failed to initialize SDL: " + std::string(SDL_GetError()));
    }

    // Create window
    gvWindow = SDL_CreateWindow("Brux GDK", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gvScrW, gvScrH, SDL_WINDOW_RESIZABLE);
    if (gvWindow == 0) {
        throw std::runtime_error("Window could not be created! SDL Error: " + std::string(SDL_GetError()));
    }

    // Create renderer for window
    gvRender = SDL_CreateRenderer(gvWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (gvRender == 0) {
        throw std::runtime_error("Renderer could not be created! SDL Error: " + std::string(SDL_GetError()));
    }

    // Initialize renderer color
    SDL_SetRenderDrawColor(gvRender, RENDERER_COLOR, RENDERER_COLOR, RENDERER_COLOR, RENDERER_COLOR);

    // Initialize PNG loading
    if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
        throw std::runtime_error("SDL_image could not initialize! SDL_image Error: " + std::string(IMG_GetError()));
    }

    // Set up the viewport
    SDL_Rect screensize;
    screensize.x = 0;
    screensize.y = 0;
    screensize.w = gvScrW;
    screensize.h = gvScrH;
    SDL_RenderSetViewport(gvRender, &screensize);
    SDL_RenderSetLogicalSize(gvRender, gvScrW, gvScrH);

    // Set the minimum window size
    SDL_SetWindowMinimumSize(gvWindow, gvScrW, gvScrH);

    // Initialize audio
    xyInitAudio();

    // Get channel count
    gvMixChannels = xyGetAudioChannels();

    // Initialize input
    xyInitInput();

    xyPrint(0, "SDL initialized successfully!");

    // Initiate Squirrel
    gvSquirrel = sq_open(SQ_STACK_SIZE);
    sq_setprintfunc(gvSquirrel, xyPrint, xyPrint);
    sq_pushroottable(gvSquirrel);

    // Register Squirrel standard libraries
    sqstd_register_iolib(gvSquirrel);
    sqstd_register_systemlib(gvSquirrel);
    sqstd_register_mathlib(gvSquirrel);
    sqstd_register_stringlib(gvSquirrel);

    // Bind all Brux API functions to Squirrel, using the generated wrapper
    xyPrint(0, "Embedding API...");
    BruxAPI::register_brux_wrapper(gvSquirrel);

    // Set error handlers for Squirrel
    sqstd_seterrorhandlers(gvSquirrel);

    xyPrint(0, "Squirrel initialized successfully!");

    // Initialize other resources
    vcTextures.push_back(0);
    vcTextureNames.push_back("");
    vcSprites.push_back(0);
    vcFonts.push_back(0);

    // Load core definitions and actors
    xyLoadCore();
    xyLoadActors();

    xyPrint(0, "\n================\n");

    // Return success
    return 1;
}

void xyEnd() {
	xyPrint(0, "\n\n================\n");

	// Cleanup all resources (except for audio)

	xyPrint(0, "Cleaning up all resources...");
	xyPrint(0, "Cleaning textures...");
	for(int i = 0; i < static_cast<int>(vcTextures.size()); i++) {
		xyDeleteImage(i);
	}

	xyPrint(0, "Cleaning sprites...");
	for(int i = 0; i < static_cast<int>(vcSprites.size()); i++) {
		delete vcSprites[i];
	}

	xyPrint(0, "Finished cleanup.");

	// Run Squirrel's garbage collector, and then close the Squirrel VM.
	
	xyPrint(0, "Closing Squirrel...");
	SQInteger garbage = sq_collectgarbage(gvSquirrel);
	xyPrint(0, "Collected %i junk obects.", garbage);
	sq_pop(gvSquirrel, 1);
	sq_close(gvSquirrel);

	// Unload all of the audio stuff

	xyPrint(0, "Unloading audio system...");
	xyUnloadAudio();

	// Close SDL

	xyPrint(0, "Closing SDL...");
	SDL_DestroyRenderer(gvRender);
	SDL_DestroyWindow(gvWindow);
	IMG_Quit();
	SDL_Quit();

	//Destroy the file system (PhysFS)
	xyPrint(0, "Closing file system...");
	xyFSDeinit();

	//Close log file
	xyPrint(0, "System closed successfully!");
	gvLog.close();
}

void xyPrint(HSQUIRRELVM v, const SQChar *s, ...) {
	va_list argv;
	va_start(argv, s);
	SQChar buffer[100*100] = _SC("");
	vsnprintf(buffer, sizeof(buffer), s, argv);
	va_end(argv);
	cout << buffer << endl;
	gvLog << buffer << endl;
}

const int NUM_BUTTONS = 32;
const int NUM_AXES = 10;
const int NUM_GAMEPADS = 8;

struct Gamepad {
    int lastButton[NUM_BUTTONS];
    int button[NUM_BUTTONS];
    int lastAxis[NUM_AXES];
    int axis[NUM_AXES];
    int hatLast;
    int hat;
    int x, y, z, h, v, r, l;
    const char* name;
};

Gamepad gamepads[NUM_GAMEPADS];

void updateButtonState(int i, int j, bool isGamepadAttached) {
    gamepads[i].lastButton[j] = gamepads[i].button[j];
    gamepads[i].button[j] = isGamepadAttached ? SDL_JoystickGetButton(gvGamepad[i], j) : 0;
}

void updateAxisState(int i, int j, bool isGamepadAttached) {
    gamepads[i].lastAxis[j] = gamepads[i].axis[j];
    gamepads[i].axis[j] = isGamepadAttached ? SDL_JoystickGetAxis(gvGamepad[i], j) : 0;
}

void xyUpdate() {
    // Update last button state
    for (int i = 0; i < 5; i++) {
        buttonlast[i] = buttonstate[i];
    }
    
    // Reset the mouse wheel position
    mouseWheelX = 0;
    mouseWheelY = 0;

    // Reset event-related globals
    gvQuit = 0;

    // Poll events
    SDL_Event Event;
    while (SDL_PollEvent(&Event)) {
        if(Event.type == SDL_QUIT) {
            gvQuit = 1;
            continue;
        }
        
        if (Event.type == SDL_MOUSEBUTTONDOWN || Event.type == SDL_MOUSEBUTTONUP) {
            int newButtonState = Event.type == SDL_MOUSEBUTTONDOWN ? 1 : 0;
            switch (Event.button.button) {
                case SDL_BUTTON_LEFT:
                    buttonstate[0] = newButtonState;
                    break;
                case SDL_BUTTON_MIDDLE:
                    buttonstate[1] = newButtonState;
                    break;
                case SDL_BUTTON_RIGHT:
                    buttonstate[2] = newButtonState;
                    break;
                case SDL_BUTTON_X1:
                    buttonstate[3] = newButtonState;
                    break;
                case SDL_BUTTON_X2:
                    buttonstate[4] = newButtonState;
                    break;
                default:
                    xyPrint(0, "Unknown button pressed! This should never happen!");
                    break;
            }
            continue;
        }
        
        if (Event.type == SDL_MOUSEWHEEL) {
            mouseWheelX = Event.wheel.x;
            mouseWheelY = Event.wheel.y;
            continue;
        }

        if (Event.type == SDL_TEXTINPUT) {
            gvInputString = Event.text.text;
            continue;
        }
    }

    // Update the game window
    SDL_RenderPresent(gvRender);
    Uint32 olddraw = gvDrawColor;
    xySetDrawColor(gvBackColor);
    SDL_RenderClear(gvRender);
    xySetDrawColor(olddraw);
    if (SDL_BYTEORDER == SDL_LIL_ENDIAN) {
        gvDrawColor = SDL_Swap32(gvDrawColor);
    }
    
    // Update input
    keylast = keystate;
    SDL_PumpEvents();
    for (int i = 0; i < 322; i++) {
        keystate[i] = sdlKeys[i];
    }
    SDL_GetMouseState(&gvMouseX, &gvMouseY);

    // Update gamepad states
    for (int i = 0; i < NUM_GAMEPADS; i++) {
        if (SDL_JoystickGetAttached(gvGamepad[i])) {
            for (int j = 0; j < NUM_BUTTONS; j++) {
                updateButtonState(i, j, true);
            }
            gamepads[i].hatLast = gamepads[i].hat;
            gamepads[i].hat = SDL_JoystickGetHat(gvGamepad[i], 0);
            for (int j = 0; j < NUM_AXES; j++) {
                updateAxisState(i, j, true);
            }
            gamepads[i].x = SDL_JoystickGetAxis(gvGamepad[i], 0);
            gamepads[i].y = SDL_JoystickGetAxis(gvGamepad[i], 1);
            gamepads[i].z = SDL_JoystickGetAxis(gvGamepad[i], 2);
            gamepads[i].h = SDL_JoystickGetAxis(gvGamepad[i], 3);
            gamepads[i].v = SDL_JoystickGetAxis(gvGamepad[i], 4);
            gamepads[i].r = (SDL_JoystickGetAxis(gvGamepad[i], 5) + 32768) / 2;
            gamepads[i].l = (SDL_JoystickGetAxis(gvGamepad[i], 2) + 32768) / 2;
            gamepads[i].name = SDL_JoystickName(gvGamepad[i]);
        } else {
            for (int j = 0; j < NUM_BUTTONS; j++) {
                updateButtonState(i, j, false);
            }
            gamepads[i].hatLast = gamepads[i].hat;
            gamepads[i].hat = 0;
            for (int j = 0; j < NUM_AXES; j++) {
                updateAxisState(i, j, false);
            }
            gamepads[i].x = 0;
            gamepads[i].y = 0;
            gamepads[i].z = 0;
            gamepads[i].h = 0;
            gamepads[i].v = 0;
            gamepads[i].r = 0;
            gamepads[i].l = 0;
            gamepads[i].name = "?";
        }
    }

    // Divide by scale
    float sx, sy;
    SDL_RenderGetScale(gvRender, &sx, &sy);
    if (1.0 > sx) {
        sx = 1.0;
    }
    if (1.0 > sy) {
        sy = 1.0;
    }
    gvMouseX /= static_cast<int>(sx);
    gvMouseY /= static_cast<int>(sy);

    // Gamepad
    // Check each pad
        for(int i = 0; i < 8; i++) {
        if(SDL_NumJoysticks() > i) gvGamepad[i] = SDL_JoystickOpen(i);
    }


// Wait for FPS limit
// Update ticks counter for FPS
#ifdef USE_CHRONO_STEADY_CLOCK
	gvTicks = std::chrono::steady_clock::now();

	std::chrono::duration<double> dLength = gvTicks - gvTickLast;
	std::chrono::duration<double> max_delay = std::chrono::duration<double>(1.0 / gvMaxFPS);
	if (dLength < max_delay) {
		if (gvMaxFPS != 0)
			std::this_thread::sleep_for((max_delay - dLength) - std::chrono::duration<double>(0.0001));
	}

	// Calculate time since previous tick and adjust framerate
	
	std::chrono::duration<double> timeSince = std::chrono::steady_clock::now() - gvTickLast;
	gvFPS = 1.0 / timeSince.count();

	// Update previous tick and increment frames
	
	gvTickLast = std::chrono::steady_clock::now();
#else
	gvTicks = SDL_GetTicks();
	Uint32 dLength = gvTicks - gvTickLast;
	Uint32 max_delay = (1000 / gvMaxFPS);

	if (dLength < max_delay) {
		if (gvMaxFPS != 0)
			SDL_Delay(max_delay - dLength);
	}

	if (dLength != 0) gvFPS = 1000 / static_cast<double>(SDL_GetTicks() - gvTickLast);
	gvTickLast = SDL_GetTicks();
#endif
	gvFrames++;
}

int xyGetOS() {
#ifdef _WIN32
	return OS_WINDOWS;
#endif
#ifdef __gnu_linux__
	return OS_LINUX;
#endif
#ifdef __ANDROID__
	return OS_ANDROID;
#endif
#ifdef __APPLE__
	return OS_MAC;
#endif
#ifdef _DINGUX
	return OS_DINGUX;
#endif
};

void __stack_chk_fail(void) {
	xyPrint(0, "Stack smash detected.");
}

