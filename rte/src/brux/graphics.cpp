//  Brux - Graphics
//  Copyright (C) 2016 KelvinShadewing
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*===============*\
| GRAPHICS SOURCE |
\*===============*/

#include "brux/main.hpp"
#include "brux/global.hpp"
#include "brux/graphics.hpp"
#include "brux/fileio.hpp"

//////////
//SYSTEM//
//////////

//Clear screen
void xyClearScreen() {
	SDL_RenderClear(gvRender);
};

// Helper function to clamp the values
int clamp(int val, int min, int max) {
    if (val < min) return min;
    if (val > max) return max;
    return val;
}

void xySetDrawColor(int r, int g, int b, int a) {
    // Set color correction
    Uint8 _r = clamp(r, 0, 255);
    Uint8 _g = clamp(g, 0, 255);
    Uint8 _b = clamp(b, 0, 255);
    Uint8 _a = clamp(a, 0, 255);

    SDL_SetRenderDrawColor(gvRender, _r, _g, _b, _a);
};

void xySetDrawColor(SQInteger color) {
    // Extract RGBA values from the color integer
    Uint8 r = (color & 0xFF000000) >> 24;
    Uint8 g = static_cast<Uint8>((color & 0x00FF0000) >> 16);
    Uint8 b = (color & 0x0000FF00) >> 8;
    Uint8 a = color & 0x000000FF;

    // Set the color
    SDL_SetRenderDrawColor(gvRender, r, g, b, a);
    gvDrawColor = static_cast<int>(color);
    if(SDL_BYTEORDER == SDL_LIL_ENDIAN) gvDrawColor = SDL_Swap32(gvDrawColor);
};

Uint8 xyGetRed(Uint32 color) {
    // Extract red component from the color integer
    Uint8 r = (color & 0xFF000000) >> 24;

    return r;
};

Uint8 xyGetGreen(Uint32 color) {
    // Extract green component from the color integer
    Uint8 g = (color & 0x00FF0000) >> 16;

    return g;
};

Uint8 xyGetBlue(Uint32 color) {
	// Extract blue component from the color integer
	Uint8 b = (color & 0x0000FF00) >> 8;

	return b;
};

Uint8 xyGetAlpha(Uint32 color) {
	Uint8 a = color & 0x000000FF;

	return a;
};

void xySetBackgroundColor(Uint32 color) {
	gvBackColor = color;
};

//....
void xyWait(int ticks) {
	SDL_Delay(ticks);
};

void xySetDrawTarget(Uint32 tex) {
    if(vcTextures.size() > tex && vcTextures[tex] != 0)
        SDL_SetRenderTarget(gvRender, vcTextures[tex]);
};

// Set draw target back to screen
void xyResetDrawTarget() {
    // gvRender is assumed to be your global SDL_Renderer object
    // SDL_SetRenderTarget with NULL as the second argument sets the target back to the default render target
    SDL_SetRenderTarget(gvRender, NULL);
};

//////////
//IMAGES//
//////////

//Load image
SDL_Texture* xyLoadTexture(const std::string& path) {
	SDL_Texture* newTexture = 0;

	//Load the surface
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if(loadedSurface == 0) {
		xyPrint(0, "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	} else {
		//Make texture from surface
		newTexture = SDL_CreateTextureFromSurface(gvRender, loadedSurface);
		if(newTexture == 0) {
			xyPrint(0, "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}

		//Delete old surface
		SDL_FreeSurface(loadedSurface);
	}

	return newTexture;
};

SDL_Texture* xyLoadTextureKeyed(const std::string& path, Uint32 key) {
    // Load the surface
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());
    if(loadedSurface == nullptr) {
        xyPrint(0, "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
        return nullptr;
    }

    // Set color key
    Uint8 r = xyGetRed(key);
    Uint8 g = xyGetGreen(key);
    Uint8 b = xyGetBlue(key);
    SDL_SetColorKey(loadedSurface, true, SDL_MapRGB(loadedSurface->format, r, g, b));

    // Create texture from surface
    SDL_Texture* newTexture = SDL_CreateTextureFromSurface(gvRender, loadedSurface);
    if(newTexture == nullptr) {
        xyPrint(0, "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
    }

    // Free the surface
    SDL_FreeSurface(loadedSurface);

    return newTexture;
};

Uint32 xyLoadImage(const std::string& path) {
    // Load the texture
    SDL_Texture* nimg = xyLoadTexture(path.c_str());
    if(!nimg) {
        xyPrint(0, "Unable to load file: %s", path.c_str());
        gvQuit = 1;
        return 0; // Return early if the texture couldn't be loaded
    }

    // Extract the name from the path
    std::string name = path.substr(path.find_last_of("/") + 1);

    // Use iterator to find the first empty slot in vcTextureNames
    auto it = std::find(vcTextureNames.begin(), vcTextureNames.end(), "");

    // If an empty slot is found, use it
    if(it != vcTextureNames.end()) {
        Uint32 i = std::distance(vcTextureNames.begin(), it);
        *it = name;
        vcTextures[i] = nimg;
        return i;
    } 

    // If no empty slot is found, add a new one
    vcTextureNames.push_back(name);
    vcTextures.push_back(nimg);
    return vcTextures.size() - 1;
};

Uint32 xyLoadImageKeyed(const std::string& path, Uint32 key) {
    // Load the texture
    SDL_Texture* nimg = xyLoadTextureKeyed(path, key);
    if(!nimg) {
        xyPrint(0, "Unable to load file: %s", path.c_str());
        gvQuit = 1;
        return 0; // Return early if the texture couldn't be loaded
    }

    // Ensure vcTextures[0] == 0
    if(vcTextures.size() == 1) vcTextures.push_back(0);

    // Add texture name
    while(vcTextureNames.size() <= 1) vcTextureNames.push_back("");

    // Extract the name from the path
    std::string name = path.substr(path.find_last_of("/") + 1);

    // Find the first empty slot in vcTextureNames
    auto it = std::find(vcTextureNames.begin() + 1, vcTextureNames.end(), "");

    // If an empty slot is found, use it
    if(it != vcTextureNames.end()) {
        Uint32 i = std::distance(vcTextureNames.begin(), it);
        *it = name;
        vcTextures[i] = nimg;
        return i;
    } 

    // If no empty slot is found, add a new one
    vcTextureNames.push_back(name);
    vcTextures.push_back(nimg);
    return vcTextures.size() - 1;
};

// Draw image
void xyDrawImage(Uint32 tex, int x, int y) {
    if(tex >= vcTextures.size() || vcTextures[tex] == nullptr) {
        return; // Return early if the texture couldn't be loaded or if the argument is out of range
    }

    SDL_Rect rec;
    rec.x = x;
    rec.y = y;

    // Query the texture size only once and use the result
    SDL_QueryTexture(vcTextures[tex], 0, 0, &rec.w, &rec.h);

    // Render the texture
    SDL_RenderCopy(gvRender, vcTextures[tex], 0, &rec);
};

void xyDrawImagePart(Uint32 tex, int x, int y, int ox, int oy, int w, int h) {
    if(tex >= vcTextures.size() || vcTextures[tex] == nullptr) {
        return; // Return early if the texture couldn't be loaded or if the argument is out of range
    }

    SDL_Rect rec = {x, y, w, h};
    SDL_Rect slice = {ox, oy, w, h};

    // Render the texture
    SDL_RenderCopy(gvRender, vcTextures[tex], &slice, &rec);
};

void xyDrawImageEx(Uint32 tex, int x, int y, float angle, SDL_RendererFlip flip, int xscale, int yscale, int alpha, Uint32 color) {
	SDL_Rect rec;
	rec.x = 0;
	rec.y = 0;

	SDL_Rect des;
	des.x = x;
	des.y = y;

	//Break color into 8-bit versions
	Uint8 r, g, b;
	r = (color >> 24) & 0xFF;
	g = (color >> 16) & 0xFF;
	b = (color >> 8) & 0xFF;

	SDL_Point piv;
	piv.x = 0;
	piv.y = 0;

	if(vcTextures.size() > tex) { //If the argument is in range)
		if(vcTextures[tex] != 0) { //If the index points to an image)
			SDL_QueryTexture(vcTextures[tex], 0, 0, &rec.w, &rec.h);
			des.w = rec.w * xscale;
			des.h = rec.h * yscale;
			SDL_SetTextureColorMod(vcTextures[tex], r, g, b);
			SDL_SetTextureAlphaMod(vcTextures[tex], alpha * 255);
			SDL_RenderCopyEx(gvRender, vcTextures[tex], &rec, &des, (double)angle, &piv, flip);
		}
	}
};

void xyDeleteImage(Uint32 tex) {
	if(tex >= vcTextures.size() || tex < 0)
		return;

	if(vcTextures[tex] != nullptr) {
		SDL_DestroyTexture(vcTextures[tex]);
		vcTextures.erase(vcTextures.begin() + tex);
	}

	if(tex < vcTextureNames.size()) {
		vcTextureNames.erase(vcTextureNames.begin() + tex);
	}
};

// Get FPS
double xyGetFPS() {
    return gvFPS;
};

Uint32 xyNewTexture(Uint32 w, Uint32 h) {
	SDL_Texture* nimg = SDL_CreateTexture( gvRender, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, w, h );

	if(!nimg) {
		xyPrint(0, "Unable to create texture!");
		gvQuit = 1;
	}

	Uint32 i;
	for(i = 1; i < vcTextureNames.size(); i++) {
		if(vcTextureNames[i] == "") {
			vcTextures[i] = nimg;
			break;
		}
	}

	if(i == vcTextureNames.size()) {
		vcTextures.emplace_back(nimg);
		vcTextureNames.emplace_back("");
	}

	vcTextureNames[i] = "new-texture-" + std::to_string(i);

	return i;
};

//////////////
// GEOMETRY //
//////////////




