/*
  Copyright (c) 2009 Dave Gamble

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

/* cJSON */
/* JSON parser in C. */

#include <string.h>
//#include <cstring.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <float.h>
#include <limits.h>
#include <ctype.h>
#include "cJSON.h"

static const char *ep;

const char *cJSON_GetErrorPtr(void) {return ep;}

static int cJSON_strcasecmp(const char *s1, const char *s2) {
    // If s1 is NULL, return 0 if s2 is also NULL, 1 otherwise
    if (!s1) {
        return (s1 == s2) ? 0 : 1;
    }
    // If s2 is NULL, return 1
    if (!s2) {
        return 1;
    }

    // Compare characters in s1 and s2 case-insensitively
    for (; *s1 && *s2; ++s1, ++s2) {
        int lower_s1 = tolower(*(const unsigned char *)s1);
        int lower_s2 = tolower(*(const unsigned char *)s2);

        if (lower_s1 != lower_s2) {
            // If characters are not equal, return the difference
            return lower_s1 - lower_s2;
        }
    }

    // If end of string is reached, return difference of null-terminated character
    return tolower(*(const unsigned char *)s1) - tolower(*(const unsigned char *)s2);
};

static void *(*cJSON_malloc)(size_t sz) = malloc;
static void (*cJSON_free)(void *ptr) = free;

static char* cJSON_strdup(const char* str) {
    // Calculate the length of the input string
    size_t strLength = strlen(str);

    // Allocate memory for the copy of the string
    char* copyStr = (char*)cJSON_malloc(strLength + 1);

    // If memory allocation failed, return NULL
    if (!copyStr) {
        return NULL;
    }

    // Copy the input string to the newly allocated memory
    // memcpy is faster when dealing with long strings, while strcpy can be faster for shorter strings
    if (strLength < 16) {
        strcpy(copyStr, str);
    } else {
        memcpy(copyStr, str, strLength + 1);
    }

    // Return the copy of the string
    return copyStr;
};

void cJSON_InitHooks(cJSON_Hooks* hooks) {
    // If hooks is NULL, reset hooks to default functions
    if (!hooks) {
        cJSON_malloc = malloc;
        cJSON_free = free;
        return;
    }

    // If malloc_fn is provided in hooks, use it. Otherwise, use default malloc
    cJSON_malloc = (hooks->malloc_fn) ? hooks->malloc_fn : malloc;

    // If free_fn is provided in hooks, use it. Otherwise, use default free
    cJSON_free = (hooks->free_fn) ? hooks->free_fn : free;
};

/* Internal constructor. */
static cJSON *cJSON_New_Item(void) {
    // Allocate memory for the new cJSON item
    cJSON* node = (cJSON*) cJSON_malloc(sizeof(cJSON));
    
    // If memory allocation was successful, initialize the memory to zero
    if (node) {
        memset(node, 0, sizeof(cJSON));
    } else {
        // Handle error here
        // For example, you might want to throw an exception
        fprintf(stderr, "Memory allocation failed in cJSON_New_Item\n");
        exit(EXIT_FAILURE); // Exit the program
    }
    
    // Return the new cJSON item
    return node;
};

/* Delete a cJSON structure. */
void cJSON_Delete(cJSON *c) {
    cJSON *next;
    while (c) {
        next = c->next;
        if (!(c->type & cJSON_IsReference) && c->child) {
            cJSON_Delete(c->child);
            c->child = NULL; // Set to NULL after deletion
        }
        if (!(c->type & cJSON_IsReference) && c->valuestring) {
            cJSON_free(c->valuestring);
            c->valuestring = NULL; // Set to NULL after freeing
        }
        if (c->str) {
            cJSON_free(c->str);
            c->str = NULL; // Set to NULL after freeing
        }
        cJSON_free(c);
        c = NULL; // Set to NULL after freeing
        c = next;
    }
};

 /* Parse the input text to generate a number, and populate the result into item. */
static const char *parse_number(cJSON *item, const char *num) {
    double n = 0, sign = 1, scale = 0;
    int subscale = 0, signsubscale = 1;

    // Check for sign
    if (*num == '-') {
        sign = -1;
        num++;
    }
    // Check if zero
    if (*num == '0') {
        num++;
    }
    // Parse number
    if (*num >= '1' && *num <= '9') {
        do {
            n = (n * 10.0) + (*num++ - '0');
        } while (*num >= '0' && *num <= '9');
    }
    // Parse fractional part
    if (*num == '.' && num[1] >= '0' && num[1] <= '9') {
        num++;
        do {
            n = (n * 10.0) + (*num++ - '0');
            scale--;
        } while (*num >= '0' && *num <= '9');
    }
    // Parse exponent
    if (*num == 'e' || *num == 'E') {
        num++;
        if (*num == '+') {
            num++;
        } else if (*num == '-') {
            signsubscale = -1;
            num++;
        }
        while (*num >= '0' && *num <= '9') {
            subscale = (subscale * 10) + (*num++ - '0');
        }
    }

    // Calculate final number: number = +/- number.fraction * 10^+/- exponent
    n = sign * n * pow(10.0, (scale + subscale * signsubscale));

    item->valuedouble = n;
    item->valueint = (int)n;
    item->type = cJSON_Number;
    return num;
};

static char *print_number(cJSON *item) {
    char *outputStr;
    double num = item->valuedouble;
    int len;

    if (num == 0) {
        len = 2;  /* special case for 0. */
    } else if (fabs(((double)item->valueint) - num) <= DBL_EPSILON && num <= INT_MAX && num >= INT_MIN) {
        len = 21;  /* integers <= INT_MAX <= 2^31-1 can be printed in 10 chars or less */
    } else {
        len = 64;  /* This is a nice tradeoff. */
    }

    outputStr = (char*) cJSON_malloc(len);
    if (!outputStr) {
        return NULL;
    }

    if (num == 0) {
        strcpy(outputStr, "0");
    } else if (fabs(((double)item->valueint) - num) <= DBL_EPSILON && num <= INT_MAX && num >= INT_MIN) {
        sprintf(outputStr, "%d", item->valueint);
    } else {
        if (fabs(floor(num) - num) <= DBL_EPSILON && fabs(num) < 1.0e60) {
            sprintf(outputStr, "%.0f", num);
        } else if (fabs(num) < 1.0e-6 || fabs(num) > 1.0e9) {
            sprintf(outputStr, "%e", num);
        } else {
            sprintf(outputStr, "%f", num);
        }
    }

    return outputStr;
};

static unsigned parse_hex4(const char *str) {
    if (str == NULL) {
        return 0;  // Null pointer error
    }

    static const int hex_values[256] = {
        ['0'] = 0, ['1'] = 1, ['2'] = 2, ['3'] = 3, ['4'] = 4,
        ['5'] = 5, ['6'] = 6, ['7'] = 7, ['8'] = 8, ['9'] = 9,
        ['A'] = 10, ['B'] = 11, ['C'] = 12, ['D'] = 13, ['E'] = 14, ['F'] = 15,
        ['a'] = 10, ['b'] = 11, ['c'] = 12, ['d'] = 13, ['e'] = 14, ['f'] = 15
    };

    unsigned h = 0;
    for (int i = 0; i < 4; ++i) {
        unsigned char c = *str++;
        if (c == '\0' || hex_values[c] == 0 && c != '0') {
            return 0;  // Invalid character in hexadecimal number
        }
        h = (h << 4) + hex_values[c];
    }

    return h;
};

static const char *parse_string(cJSON *item, const char *str) {
    if (str == NULL || item == NULL) {
        return 0;  // Null pointer error
    }

    static const unsigned char firstByteMark[7] = { 0x00, 0x00, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC };
    static const char lookup[256] = {
        ['\"'] = 1, ['\\'] = 1, ['\b'] = 1, ['\f'] = 1,
        ['\n'] = 1, ['\r'] = 1, ['\t'] = 1
    };
    const char *ptr = str + 1;
    char *out, *ptr2;
    int len = 0;
    unsigned uc, uc2;

    if (*str != '\"') {
        ep = str;
        return 0;
    }

    while (*ptr != '\"' && *ptr) {
        if (*ptr != '\\') {
            ptr++;
            len++;
        } else {
            ptr++;
            if (*ptr == 'u') {
                ptr += 4;
                len += 4;
            } else {
                ptr++;
                len++;
            }
        }
    }

    out = (char*) cJSON_malloc(len + 1);
    if (!out) {
        return 0;
    }

    ptr = str + 1;
    ptr2 = out;
    while (*ptr != '\"' && *ptr) {
        if (*ptr != '\\') {
            *ptr2++ = *ptr++;
        } else {
            ptr++;
            switch (*ptr) {
                case 'b': *ptr2++ = '\b'; break;
                case 'f': *ptr2++ = '\f'; break;
                case 'n': *ptr2++ = '\n'; break;
                case 'r': *ptr2++ = '\r'; break;
                case 't': *ptr2++ = '\t'; break;
                case 'u':  /* Transcode utf16 to utf8. */
                    if (sscanf(ptr + 1, "%4x", &uc) != 1) {
                        cJSON_free(out);
                        return 0;  // Invalid unicode sequence
                    }
                    ptr += 4;

                    if ((uc >= 0xDC00 && uc <= 0xDFFF) || uc == 0) {
                        break;
                    }

                    if (uc >= 0xD800 && uc <= 0xDBFF) {
                        if (ptr[1] != '\\' || ptr[2] != 'u') {
                            break;
                        }
                        if (sscanf(ptr + 3, "%4x", &uc2) != 1) {
                            cJSON_free(out);
                            return 0;  // Invalid unicode sequence
                        }
                        ptr += 6;
                        if (uc2 < 0xDC00 || uc2 > 0xDFFF) {
                            break;
                        }
                        uc = 0x10000 + (((uc & 0x3FF) << 10) | (uc2 & 0x3FF));
                    }

                    len = 4;
                    if (uc < 0x80) {
                        len = 1;
                    } else if (uc < 0x800) {
                        len = 2;
                    } else if (uc < 0x10000) {
                        len = 3;
                    }
                    ptr2 += len;

                    switch (len) {
                        case 4: *--ptr2 = ((uc | 0x80) & 0xBF); uc >>= 6;
                        case 3: *--ptr2 = ((uc | 0x80) & 0xBF); uc >>= 6;
                        case 2: *--ptr2 = ((uc | 0x80) & 0xBF); uc >>= 6;
                        case 1: *--ptr2 = (uc | firstByteMark[len]);
                    }
                    ptr2 += len;
                    break;
                default:  *ptr2++ = *ptr; break;
            }
            ptr++;
        }
    }
    *ptr2 = 0;
    if (*ptr == '\"') {
        ptr++;
    }
    item->valuestring = out;
    item->type = cJSON_String;
    return ptr;
};

static char *print_string_ptr(const char *str) {
    if (str == NULL) {
        return NULL;  // Null pointer error
    }

    static const char lookup[256] = {
        ['\"'] = 1, ['\\'] = 1, ['\b'] = 1, ['\f'] = 1,
        ['\n'] = 1, ['\r'] = 1, ['\t'] = 1
    };
    const char *ptr = str;
    char *out, *ptr2;
    int len = 0;
    unsigned char token;

    while ((token = *ptr++)) {
        len += 1 + lookup[token] + (token < 32) * 5;
    }

    out = (char*) cJSON_malloc(len + 3);
    if (!out) {
        return NULL;  // Memory allocation error
    }

    ptr2 = out;
    ptr = str;
    *ptr2++ = '\"';
    while ((token = *ptr++)) {
        if (token > 31 && !lookup[token]) {
            *ptr2++ = token;
        } else {
            *ptr2++ = '\\';
            switch (token) {
                case '\\': *ptr2++ = '\\'; break;
                case '\"': *ptr2++ = '\"'; break;
                case '\b': *ptr2++ = 'b'; break;
                case '\f': *ptr2++ = 'f'; break;
                case '\n': *ptr2++ = 'n'; break;
                case '\r': *ptr2++ = 'r'; break;
                case '\t': *ptr2++ = 't'; break;
                default: 
                    ptr2 += sprintf(ptr2, "u%04x", token);
                    break;
            }
        }
    }
    *ptr2++ = '\"';
    *ptr2 = 0;
    return out;
};

/* Invote print_string_ptr (which is useful) on an item. */
static char *print_string(cJSON *item)	{return print_string_ptr(item->valuestring);}

/* Predeclare these prototypes. */
static const char *parse_value(cJSON *item, const char *value);

static char *print_value(cJSON *item, int depth, int fmt);

static const char *parse_array(cJSON *item, const char *value);

static char *print_array(cJSON *item, int depth, int fmt);

static const char *parse_object(cJSON *item, const char *value);

static char *print_object(cJSON *item, int depth, int fmt);

/* Utility to jump whitespace and cr/lf */
static const char *skip(const char *in) {
    if (in == NULL) {
        return NULL;
    }

    while (*in && (unsigned char)*in <= 32) {
        in++;
    }

    return in;
};

cJSON *cJSON_ParseWithOpts(const char *value, const char **return_parse_end, int require_null_terminated) {
    if (value == NULL) {
        printf("Error: Passed NULL value pointer\n");
        return NULL;
    }

    cJSON *c = cJSON_New_Item();
    if (!c) {
        /* Memory allocation failed */
        printf("Error: Memory allocation failed\n");
        return NULL;
    }

    value = skip(value);
    if (value == NULL) {
        /* Failed to skip leading whitespace */
        printf("Error: Failed to skip leading whitespace\n");
        cJSON_Delete(c);
        return NULL;
    }

    const char *end = parse_value(c, value);
    if (!end) {
        /* Parse failure. ep is set. */
        printf("Error: Failed to parse value\n");
        cJSON_Delete(c);
        return NULL;
    }

    /* If we require null-terminated JSON without appended garbage, skip and then check for a null terminator */
    if (require_null_terminated) {
        end = skip(end);
        if (end == NULL) {
            /* Failed to skip trailing whitespace */
            printf("Error: Failed to skip trailing whitespace\n");
            cJSON_Delete(c);
            return NULL;
        }

        if (*end) {
            printf("Error: JSON not null-terminated\n");
            cJSON_Delete(c);
            return NULL;
        }
    }

    if (return_parse_end) {
        *return_parse_end = end;
    }

    return c;
};

/* Default options for cJSON_Parse */
cJSON *cJSON_Parse(const char *value) {
    if (value == NULL) {
        printf("Error: Passed NULL value pointer\n");
        return NULL;
    }

    cJSON *item = cJSON_ParseWithOpts(value, 0, 0);

    if (item == NULL) {
        printf("Error: Failed to parse JSON\n");
    }

    return item;
};

/* Render a cJSON item/entity/structure to text. */
char *cJSON_Print(cJSON *item) {
    if (item == NULL) {
        printf("Error: Passed NULL item pointer\n");
        return NULL;
    }
    return print_value(item, 0, 1);
};

char *cJSON_PrintUnformatted(cJSON *item) {
    if (item == NULL) {
        printf("Error: Passed NULL item pointer\n");
        return NULL;
    }
    return print_value(item, 0, 0);
};

static const char *parse_value(cJSON *item, const char *value) {
    if (!value) {
        /* Fail on null. */
        return NULL;
    }

    switch (*value) {
        case 'n':  /* null */
            if (!strncmp(value, "null", 4)) {
                item->type = cJSON_NULL;
                return value + 4;
            }
            break;
        case 'f':  /* false */
            if (!strncmp(value, "false", 5)) {
                item->type = cJSON_False;
                return value + 5;
            }
            break;
        case 't':  /* true */
            if (!strncmp(value, "true", 4)) {
                item->type = cJSON_True;
                item->valueint = 1;
                return value + 4;
            }
            break;
        case '\"':  /* string */
            return parse_string(item, value);
        case '-':  /* number */
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            return parse_number(item, value);
        case '[':  /* array */
            return parse_array(item, value);
        case '{':  /* object */
            return parse_object(item, value);
        default:  /* failure */
            ep = value;
            return NULL;
    }

    /* Failure. */
    ep = value;
    return NULL;
};

static char *print_value(cJSON *item, int depth, int fmt) {
    char *out = NULL;

    switch ((item ? item->type : 0) & 255) {
        case cJSON_NULL:
            out = "null";
            break;
        case cJSON_False:
            out = "false";
            break;
        case cJSON_True:
            out = "true";
            break;
        case cJSON_Number:
            out = print_number(item);
            break;
        case cJSON_String:
            out = print_string(item);
            break;
        case cJSON_Array:
            out = print_array(item, depth, fmt);
            break;
        case cJSON_Object:
            out = print_object(item, depth, fmt);
            break;
        default:
            /* Fail on null. */
            return NULL;
    }

    return out;
};

static const char *parse_array(cJSON *item, const char *value) {
    cJSON *child;

    if (*value != '[') {
        /* Not an array! */
        ep = value;
        return NULL;
    }

    item->type = cJSON_Array;
    value = skip(value + 1);

    if (*value == ']') {
        /* Empty array. */
        return value + 1;
    }

    item->child = child = cJSON_New_Item();
    if (!item->child) {
        /* Memory allocation failed */
        return NULL;
    }

    /* Skip any spacing, get the value. */
    value = parse_value(child, skip(value));
    if (!value) {
        return NULL;
    }

    value = skip(value);

    while (*value == ',') {
        cJSON *new_item;

        if (!(new_item = cJSON_New_Item())) {
            /* Memory allocation failed */
            return NULL;
        }

        child->next = new_item;
        new_item->prev = child;
        child = new_item;

        value = parse_value(child, skip(value + 1));
        if (!value) {
            /* Memory allocation failed */
            return NULL;
        }

        value = skip(value);
    }

    if (*value == ']') {
        /* End of array */
        return value + 1;
    }

    /* Malformed array */
    ep = value;
    return NULL;
};

static char *print_array(cJSON *item, int depth, int fmt) {
    cJSON *child = item->child;
    int numentries = 0, i = 0, fail = 0;
    size_t len = 5;
    char *out = NULL, *ptr, *ret;

    /* Count the number of entries in the array */
    while (child) {
        numentries++;
        child = child->next;
    }

    /* Handle empty array */
    if (!numentries) {
        out = (char*)cJSON_malloc(3);
        if (out) {
            strcpy(out, "[]");
        }
        return out;
    }

    /* Allocate an array to hold the values for each entry */
    size_t *entry_lengths = (size_t*)cJSON_malloc(numentries * sizeof(size_t));
    if (!entry_lengths) {
        return NULL;
    }

    /* Retrieve all the results */
    child = item->child;
    while (child && !fail) {
        ret = print_value(child, depth + 1, fmt);
        if (ret) {
            entry_lengths[i] = strlen(ret);
            len += entry_lengths[i] + 2 + (fmt ? 1 : 0);
            i++;
        } else {
            fail = 1;
        }
        child = child->next;
    }

    /* If no failure, try to allocate the output string */
    if (!fail) {
        out = (char*)cJSON_malloc(len);
        if (!out) {
            fail = 1;
        }
    }

    /* Handle failure */
    if (fail) {
        cJSON_free(entry_lengths);
        return NULL;
    }

    /* Compose the output array */
    *out = '[';
    ptr = out + 1;
    *ptr = 0;
    child = item->child;
    for (i = 0; i < numentries; i++) {
        ret = print_value(child, depth + 1, fmt);
        strcpy(ptr, ret);
        ptr += entry_lengths[i];
        if (i != numentries - 1) {
            *ptr++ = ',';
            if (fmt) {
                *ptr++ = ' ';
            }
            *ptr = 0;
        }
        cJSON_free(ret);
        child = child->next;
    }
    cJSON_free(entry_lengths);
    *ptr++ = ']';
    *ptr++ = 0;

    return out;
};

static const char *parse_object(cJSON *item, const char *value) {
    cJSON *child;
    char *child_value;
    int child_type;

    if (*value != '{') {
        ep = value;
        return NULL;
    }

    item->type = cJSON_Object;
    value = skip(value + 1);
    if (!value) {
        return NULL; // Error: unexpected end of input
    }
    if (*value == '}') {
        return value + 1;
    }

    do {
        cJSON *new_item = cJSON_New_Item();
        if (!new_item) {
            return NULL; // Error: failed to allocate memory for new item
        }

        if (item->child) {
            child->next = new_item;
            new_item->prev = child;
            child = new_item;
        } else {
            item->child = child = new_item;
        }

        value = skip(parse_string(child, skip(value)));
        if (!value) {
            return NULL; // Error: failed to parse string
        }

        child_value = child->valuestring;
        child_type = child->type;
        child->str = child_value;
        child->valuestring = NULL;

        if (*value != ':') {
            ep = value;
            return NULL; // Error: expected ':' after key in object
        }

        value = skip(parse_value(child, skip(value + 1)));
        if (!value) {
            return NULL; // Error: failed to parse value
        }
    } while (*value == ',' && (value = skip(value + 1)));

    if (*value == '}') {
        return value + 1;
    }

    ep = value;
    return NULL; // Error: expected '}' at end of object
};

typedef struct {
    char *name;
    char *entry;
    int name_len;
    int entry_len;
} NameEntry;

static char *print_object(cJSON *item, int depth, int fmt) {
    cJSON *child = item->child;
    int num_entries = 0;
    size_t total_len = 0;
    size_t *child_lens = NULL;

    /* Count the number of entries and calculate total length. */
    while (child) {
        char *child_str = print_value(child, depth, fmt);
        if (!child_str) {
            free(child_lens);
            return NULL; // Error: failed to print value
        }
        num_entries++;
        child_lens = (size_t*)realloc(child_lens, sizeof(size_t) * num_entries);
        child_lens[num_entries - 1] = strlen(child_str);
        total_len += strlen(child->str) + child_lens[num_entries - 1] + (fmt ? depth + 4 : 3);
        cJSON_free(child_str);
        child = child->next;
    }

    /* Allocate space for the output string */
    char *output = (char*)cJSON_malloc(total_len + 1); // +1 for null terminator
    if (!output) {
        free(child_lens);
        return NULL; // Error: failed to allocate memory for output
    }

    /* Compose the output: */
    char *ptr = output;
    *ptr++ = '{';
    if (fmt) {
        *ptr++ = '\n';
        for (int i = 0; i < depth - 1; i++) *ptr++ = '\t';
    }
    *ptr++ = '}';

    child = item->child;
    depth++;
    for (int i = 0; i < num_entries; i++) {
        char *name = print_string_ptr(child->str);
        char *entry = print_value(child, depth, fmt);
        if (name && entry) {
            if (fmt) for (int j = 0; j < depth; j++) *ptr++ = '\t';
            size_t name_len = strlen(name);
            memcpy(ptr, name, name_len);
            ptr += name_len;
            *ptr++ = ':';
            if (fmt) *ptr++ = '\t';
            memcpy(ptr, entry, child_lens[i]);
            ptr += child_lens[i];
            if (i != num_entries - 1) *ptr++ = ',';
            if (fmt) *ptr++ = '\n';
        } else {
            cJSON_free(output); // Error: failed to print name or entry
            free(child_lens);
            return NULL;
        }
        cJSON_free(name);
        cJSON_free(entry);
        child = child->next;
    }

    if (fmt) for (int i = 0; i < depth - 1; i++) *ptr++ = '\t';
    *ptr++ = '}';
    *ptr = 0; // null terminate the string

    free(child_lens);
    return output;
};

/* Get Array size/item / object item. */
int cJSON_GetArraySize(cJSON *array) {
    /* Check if the input array is NULL */
    if (array == NULL) {
        return -1; // Error: input array is NULL
    }

    cJSON *c = array->child;
    int i = 0;
    while (c) {
        i++;
        c = c->next;
    }
    return i;
};

cJSON *cJSON_GetArrayItem(cJSON *array, int item) {
    if (array == NULL) {
        printf("Error: Passed NULL array pointer\n");
        return NULL;
    }
    if (item < 0) {
        printf("Error: Negative index passed\n");
        return NULL;
    }

    cJSON *c = array->child;
    while (c && item > 0) {
        item--;
        c = c->next;
    }

    if (item > 0) {
        printf("Error: Index out of bounds\n");
        return NULL;
    }

    return c;
};

cJSON *cJSON_GetObjectItem(cJSON *object, const char *str) {
    if (object == NULL) {
        printf("Error: NULL input object\n");
        return NULL;
    }

    if (str == NULL) {
        printf("Error: NULL input string\n");
        return NULL;
    }

    cJSON *c = object->child;
    while (c && cJSON_strcasecmp(c->str, str)) {
        c = c->next;
    }

    if (c == NULL) {
        printf("Error: No matching item found\n");
    }

    return c;
};

/* Utility for array list handling. */
static void suffix_object(cJSON *prev, cJSON *item) {
    if (prev == NULL || item == NULL) {
        printf("Error: NULL input provided.\n");
        return;
    }
    prev->next = item;
    item->prev = prev;
};

/* Utility for handling references. */
static cJSON *create_reference(cJSON *item) {
    if (item == NULL) {
        printf("Error: NULL input item\n");
        return NULL;
    }

    cJSON *ref = cJSON_New_Item();
    if (!ref) {
        printf("Error: Memory allocation failed\n");
        return NULL;
    }

    //ref->str = 0;
    memcpy(ref, item, sizeof(cJSON));
    if (ref->type & cJSON_IsReference) {
        printf("Error: Item is already a reference\n");
        free(ref);
        return NULL;
    }

    ref->type |= cJSON_IsReference;
    ref->next = ref->prev = 0;
    return ref;
};

/* Add item to array/object. */
void cJSON_AddItemToArray(cJSON *array, cJSON *item) {
    if (array == NULL || item == NULL) {
        printf("Error: NULL input provided.\n");
        return;
    }
    cJSON *c = array->child;
    if (!c) {
        array->child = item;
    } else {
        while (c && c->next) {
            c = c->next;
        }
        if (c) {
            suffix_object(c, item);
        } else {
            printf("Error: Failed to find the last item in the array.\n");
        }
    }
};

void cJSON_AddItemToObject(cJSON *object, const char *str, cJSON *item) {
    if (object == NULL || str == NULL || item == NULL) {
        printf("Error: NULL input provided.\n");
        return;
    }
    if (item->str) {
        cJSON_free(item->str);
    }
    char *new_str = cJSON_strdup(str);
    if (new_str == NULL) {
        printf("Error: Failed to duplicate string.\n");
        return;
    }
    item->str = new_str;
    cJSON_AddItemToArray(object, item);
};

void cJSON_AddItemReferenceToArray(cJSON *array, cJSON *item) {
    if (array == NULL || item == NULL) {
        printf("Error: NULL input provided.\n");
        return;
    }
    cJSON *reference = create_reference(item);
    if (reference) {
        cJSON_AddItemToArray(array, reference);
    } else {
        printf("Error: Failed to create reference.\n");
    }
};

void cJSON_AddItemReferenceToObject(cJSON *object, const char *str, cJSON *item) {
    if (object == NULL || str == NULL || item == NULL) {
        printf("Error: NULL input provided.\n");
        return;
    }
    cJSON *reference = create_reference(item);
    if (reference) {
        cJSON_AddItemToObject(object, str, reference);
    } else {
        printf("Error: Failed to create reference.\n");
    }
};

cJSON *cJSON_DetachItemFromArray(cJSON *array, int which) {
    if (array == NULL) {
        printf("Error: NULL array provided.\n");
        return NULL;
    }
    if (which < 0 || which >= cJSON_GetArraySize(array)) {
        printf("Error: Index out of bounds.\n");
        return NULL;
    }

    cJSON *c = array->child;
    while (c && which > 0) {
        c = c->next;
        which--;
    }

    if (!c) {
        printf("Error: Item not found in array.\n");
        return NULL;
    }

    if (c->prev) c->prev->next = c->next;
    if (c->next) c->next->prev = c->prev;
    if (c == array->child) array->child = c->next;
    c->prev = c->next = 0;

    return c;
};

void cJSON_DeleteItemFromArray(cJSON *array, int which) {
    if (array == NULL) {
        printf("Error: NULL array provided.\n");
        return;
    }
    if (which < 0 || which >= cJSON_GetArraySize(array)) {
        printf("Error: Index out of bounds.\n");
        return;
    }
    cJSON *item = cJSON_DetachItemFromArray(array, which);
    if (item) {
        cJSON_Delete(item);
    } else {
        printf("Error: Item not found in array.\n");
    }
};

cJSON *cJSON_DetachItemFromObject(cJSON *object, const char *str) {
    if (object == NULL || str == NULL) {
        printf("Error: NULL input provided.\n");
        return NULL;
    }
    if (object->child == NULL) {
        printf("Error: Object has no children.\n");
        return NULL;
    }

    int i = 0;
    cJSON *c = object->child;
    while (c && cJSON_strcasecmp(c->str, str)) {
        i++;
        c = c->next;
    }

    if (c) {
        return cJSON_DetachItemFromArray(object, i);
    } else {
        printf("Error: Item not found in object.\n");
        return NULL;
    }
};

void cJSON_DeleteItemFromObject(cJSON *object, const char *str) {
    if (object == NULL || str == NULL) {
        printf("Error: NULL input provided.\n");
        return;
    }
    cJSON *item = cJSON_DetachItemFromObject(object, str);
    if (item) {
        cJSON_Delete(item);
    } else {
        printf("Error: Item not found in object.\n");
    }
};

/* Replace array/object items with new ones. */
void cJSON_ReplaceItemInArray(cJSON *array, int which, cJSON *newitem) {
    if (array == NULL || newitem == NULL) {
        fprintf(stderr, "Error: Null pointer argument.\n");
        return;
    }
    if (which < 0) {
        fprintf(stderr, "Error: Index out of bounds.\n");
        return;
    }

    cJSON *c = array->child;
    while (c && which > 0) {
        c = c->next;
        which--;
    }

    if (!c) {
        fprintf(stderr, "Error: Item not found in array.\n");
        return;
    }

    newitem->next = c->next;
    newitem->prev = c->prev;
    if (newitem->next) {
        newitem->next->prev = newitem;
    }
    if (c == array->child) {
        array->child = newitem;
    } else {
        newitem->prev->next = newitem;
    }
    c->next = c->prev = 0;
    cJSON_Delete(c);
};

#define MAX_STR_LEN 100

void cJSON_ReplaceItemInObject(cJSON *object, const char *str, cJSON *newitem) {
    if (object == NULL || str == NULL || newitem == NULL) {
        fprintf(stderr, "Error: Null pointer argument.\n");
        return;
    }

    int i = 0;
    cJSON *c = object->child;
    while (c && strncmp(c->str, str, MAX_STR_LEN)) {
        i++;
        c = c->next;
    }

    if (c) {
        newitem->str = cJSON_strdup(str);
        if (newitem->str == NULL) {
            fprintf(stderr, "Error: cJSON_strdup() failed to allocate memory.\n");
            return;
        }
        cJSON_ReplaceItemInArray(object, i, newitem);
    } else {
        fprintf(stderr, "Error: Item not found in object.\n");
    }
};

/* Create basic types: */
cJSON *cJSON_CreateNull(void) {
    cJSON *item = cJSON_New_Item();
    if (item == NULL) {
        fprintf(stderr, "Error: cJSON_New_Item() failed to allocate memory.\n");
        return NULL;
    }
    item->type = cJSON_NULL;
    return item;
};

cJSON *cJSON_CreateTrue(void) {
    cJSON *item = cJSON_New_Item();
    if (item == NULL) {
        return NULL; // Return NULL if memory allocation failed
    }

    item->type = cJSON_True;

    return item;
};

cJSON *cJSON_CreateFalse(void) {
    cJSON *item = cJSON_New_Item();
    if (item == NULL) {
        return NULL; // Return NULL if memory allocation failed
    }

    item->type = cJSON_False;

    return item;
};

cJSON *cJSON_CreateBool(int b) {
    cJSON *item = cJSON_New_Item();
    if (item == NULL) {
        return NULL; // Return NULL if memory allocation failed
    }

    item->type = b ? cJSON_True : cJSON_False;

    return item;
};

cJSON *cJSON_CreateNumber(double num) {
    cJSON *item = cJSON_New_Item();
    if (item == NULL) {
        return NULL; // Return NULL if memory allocation failed
    }

    item->type = cJSON_Number;
    item->valuedouble = num;
    item->valueint = (int)num;

    return item;
};

cJSON *cJSON_CreateString(const char *str) {
    if (str == NULL) {
        return NULL; // Return NULL if the input string is NULL
    }

    cJSON *item = cJSON_New_Item();
    if (item == NULL) {
        return NULL; // Return NULL if memory allocation failed
    }

    item->type = cJSON_String;
    item->valuestring = cJSON_strdup(str);
    if (item->valuestring == NULL) {
        cJSON_Delete(item); // Free memory if string duplication failed
        return NULL;
    }

    return item;
};

cJSON *cJSON_CreateArray(void) {
    cJSON *item = cJSON_New_Item();
    if (item == NULL) {
        return NULL;
    }
    item->type = cJSON_Array;
    return item;
};

cJSON *cJSON_CreateObject(void) {
    cJSON *item = cJSON_New_Item();
    if (item) {
        item->type = cJSON_Object;
    }
    return item;
};

cJSON *cJSON_CreateIntArray(const int *numbers,int count) {
    cJSON *a = cJSON_CreateArray();
    if (!a) return NULL;

    cJSON *last = NULL;
    for (int i = 0; i < count; i++) {
        cJSON *n = cJSON_CreateNumber(numbers[i]);
        if (!n) {
            cJSON_Delete(a);
            return NULL;
        }

        if (last) {
            last->next = n;
            n->prev = last;
        } else {
            a->child = n;
        }

        last = n;
    }

    return a;
};

// Similar changes can be made to cJSON_CreateFloatArray, cJSON_CreateDoubleArray, and cJSON_CreateStringArray

char *cJSON_strdup_once(const char *str) {
    if (str == NULL) {
        return NULL;
    }
    size_t len = strlen(str) + 1;
    char *new_str = (char*)malloc(len);
    if (new_str == NULL) {
        return NULL;
    }
    memcpy(new_str, str, len);
    return new_str;
};

cJSON *cJSON_Duplicate(cJSON *item, int recurse) {
    if (!item) return NULL;

    cJSON *newitem = cJSON_New_Item();
    if (!newitem) return NULL;

    newitem->type = item->type & (~cJSON_IsReference);
    newitem->valueint = item->valueint;
    newitem->valuedouble = item->valuedouble;

    if (item->valuestring) {
        newitem->valuestring = cJSON_strdup_once(item->valuestring);
        if (!newitem->valuestring) {
            cJSON_Delete(newitem);
            return NULL;
        }
    }

    if (item->str) {
        newitem->str = cJSON_strdup_once(item->str);
        if (!newitem->str) {
            cJSON_Delete(newitem);
            return NULL;
        }
    }

    if (!recurse) return newitem;

    cJSON *prevchild = NULL;
    for (cJSON *child = item->child; child != NULL; child = child->next) {
        cJSON *newchild = cJSON_Duplicate(child, 1);
        if (!newchild) {
            cJSON_Delete(newitem);
            return NULL;
        }

        if (prevchild) {
            prevchild->next = newchild;
            newchild->prev = prevchild;
        } else {
            newitem->child = newchild;
        }

        prevchild = newchild;
    }

    return newitem;
};

void cJSON_Minify(char *source) {
    if (source == NULL) {
        return;
    }

    static const unsigned char symbol[256] = {
        [' '] = 1, ['\t'] = 1, ['\r'] = 1, ['\n'] = 1, ['/'] = 2, ['\"'] = 3
    };

    char *destination = source;
    char *segment_start = NULL;

    while (*source) {
        switch (symbol[(unsigned char)*source]) {
            case 1:
                // Skip whitespace characters.
                if (segment_start) {
                    memmove(destination, segment_start, source - segment_start);
                    destination += source - segment_start;
                    segment_start = NULL;
                }
                source++;
                break;
            case 2:
                // Skip comments.
                if (source[1] == '/' || source[1] == '*') {
                    if (segment_start) {
                        memmove(destination, segment_start, source - segment_start);
                        destination += source - segment_start;
                        segment_start = NULL;
                    }
                    char comment_end = source[1] == '/' ? '\n' : '/';
                    do {
                        source++;
                    } while (*source && *source != comment_end);
                    source = *source ? source + 1 : source;
                } else if (!segment_start) {
                    segment_start = source;
                }
                source++;
                break;
            case 3:
                // Copy string literals, which are \" sensitive.
                if (segment_start) {
                    memmove(destination, segment_start, source - segment_start);
                    destination += source - segment_start;
                }
                segment_start = source;
                do {
                    source++;
                } while (*source && *source != '\"');
                source = *source ? source + 1 : source;
                break;
            default:
                // Start a new segment if necessary.
                if (!segment_start) {
                    segment_start = source;
                }
                source++;
                break;
        }
    }

    if (segment_start) {
        memmove(destination, segment_start, source - segment_start);
        destination += source - segment_start;
    }

    // Null-terminate the destination string.
    *destination = 0;
};
