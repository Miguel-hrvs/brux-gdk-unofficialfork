//  Brux - File I/O API
//  Copyright (C) 2016 KelvinShadewing
//                2023 Vankata453
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "api/fileio.hpp"

#include "brux/main.hpp"

#include "brux/fileio.hpp"
#include "brux/global.hpp"
#include <unordered_map>

namespace BruxAPI {

/////////////
// FILE IO //
/////////////

void import(const std::string& file) {
	donut(file);
}

void donut(const std::string& file) {
	/*
	std::string b = "";
	char c[256];
	if(getcwd(c, sizeof(c)) == 0) return 0;
	b += c;
	b += "/";
	b += a;
	*/

	xyPrint(0, "Running %s...", file.c_str());
	sqstd_dofile(gvSquirrel, file.c_str(), 0, 1);
}

std::unordered_map<std::string, HSQOBJECT> scriptCache;

void dostr(const std::string& str) {
    SQInteger oldtop = sq_gettop(gvSquirrel);
    
    // Check if the script is in the cache
    auto it = scriptCache.find(str);
    if (it != scriptCache.end()) {
        // If the script is in the cache, push it onto the stack
        sq_pushobject(gvSquirrel, it->second);
    } else {
        // If the script is not in the cache, compile and cache it
        sq_compilebuffer(gvSquirrel, str.c_str(), static_cast<int>(str.size()) * sizeof(SQChar), "std::string", 1);
        HSQOBJECT script;
        sq_getstackobj(gvSquirrel, -1, &script);
        sq_addref(gvSquirrel, &script);
        scriptCache[str] = script;
    }
    
    sq_pushroottable(gvSquirrel);
    sq_call(gvSquirrel, 1, SQFalse, SQTrue);
    sq_settop(gvSquirrel, oldtop);
}


void mount(const std::string& dir, const std::string& mountpoint, bool prepend) {
	xyFSMount(dir, mountpoint, prepend);
}

void unmount(const std::string& dir) {
	xyFSUnmount(dir);
}

std::string getdir() {
	return xyGetDir();
}

std::string getWriteDir() {
	return xyGetWriteDir();
}

std::string getPrefDir(const std::string& org, const std::string& app) {
	return xyGetPrefDir(org, app);
}

void setWriteDir(const std::string& dir) {
	xySetWriteDir(dir);
}

void createDir(const std::string& name) {
	xyCreateDir(name);
}

std::unordered_map<std::string, std::string> fileCache;

std::string fileRead(const std::string& file) {
    // Check if the file is in the cache
    auto it = fileCache.find(file);
    if (it != fileCache.end()) {
        // If the file is in the cache, return the cached content
        return it->second;
    }

    // If the file is not in the cache, read it and add it to the cache
    if (xyFileExists(file)) {
        std::string content = xyFileRead(file);
        fileCache[file] = content;
        return content;
    }

    xyPrint(0, "WARNING: %s does not exist!", file.c_str());
    return "-1";
}

void fileWrite(const std::string& file, const std::string& data) {
	xyFileWrite(file, data);
}

void fileAppend(const std::string& file, const std::string& data) {
	xyFileAppend(file, data);
}

bool fileExists(const std::string& file) {
	return xyFileExists(file);
}

void fileDelete(const std::string& file) {
	xyFileDelete(file);
}

bool isdir(const std::string& dir) {
	return xyIsDirectory(dir);
}

SQInteger lsdir(HSQUIRRELVM v, const std::string& dir) {
    // Create array for results.
    sq_newarray(v, 0);

    // Append all results to the array.
    const std::vector<std::string> files = xyListDirectory(dir);
    std::string file;
    for (size_t i = 0; i < files.size(); ++i) {
        file = files[i];
        sq_pushstring(v, file.c_str(), file.size());
        sq_arrayappend(v, -2);
    }

    return 1; // Returns data.
}

} // namespace BruxAPI
