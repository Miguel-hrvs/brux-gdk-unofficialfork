//  Brux - Maths API
//  Copyright (C) 2016 KelvinShadewing
//                2023 Vankata453
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "api/maths.hpp"
#include "brux/main.hpp"
#include "brux/maths.hpp"

namespace BruxAPI {

///////////
// MATHS //
///////////

float randFloat(float a) {
	return xyRandomFloat(a);
}

int randInt(int a) {
	return xyRandomInt(a);
}

float distance2(float x1, float y1, float x2, float y2) {
	return xyDistance(x1, y1, x2, y2);
}

bool inDistance2(float x1, float y1, float x2, float y2, float d) {
	return xyInDistance2(x1, y1, x2, y2, d);
}

float distance3(float x1, float y1, float z1, float x2, float y2, float z2) {
	return xyDistance3(x1, y1, z1, x2, y2, z2);
}

float wrap(float x, float mx, float mn) {
	return xyWrap(x, mn, mx);
}

int floor(float f) {
	return static_cast<int>(std::floor(f));
}

int ceil(float f) {
	return static_cast<int>(std::ceil(f));
}

int round(float f) {
	return static_cast<int>(std::round(f));
}

float pointAngle(float x1, float y1, float x2, float y2) {
	return xyPointAngle(x1, y1, x2, y2);
}

float abs(float f) {
	return std::abs(f);
}

float lendirX(float l, float d) {
	return xyLenDirX(l, d);
}

float lendirY(float l, float d) {
	return xyLenDirY(l, d);
}

std::string binstr(int i) {
	return std::bitset<64>(i).to_string();
}

} // namespace BruxAPI
